<?php

/**
 * @file
 * Prepares tables ready for tracking which nodes get translated by Sprawk
 */

/**
 * Implements hook_schema().
 */
function sprawk_schema() {
  $schema['sprawk_node'] = array(
  	'description' => 'Stores per-node translation preferences',
    'fields' => array(
      'nid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0),
      'setting' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0)
    ),
    'primary key' => array('nid'),
  );
  $schema['cache_sprawk'] = array(
    'fields' => array(
      'cid' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''),
      'data' => array(
        'type' => 'blob',
        'size' => 'big'),
      'expire' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0),
      'created' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0),
      'headers' => array(
        'type' => 'text',
        'not null' => FALSE),
      'serialized' => array(
        'type' => 'int',
        'size' => 'small',
        'not null' => TRUE,
        'default' => 0)
    ),
    'primary key' => array('cid'),
    'indexes' => array(
      'expire' => array('expire')
    )
  );
  $schema['sprawk_log'] = array(
  	'description' => 'Stores logs of API for later analysis',
    'fields' => array(
      'date' => array(
      	'description' => 'The time this call was made',
        'type' => 'datetime',
        'not null' => TRUE),
      'uri' => array(
      	'description' => 'The URI of the API call',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE),
      'tl' => array(
      	'description' => 'The target language code',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE),
      'resultcode' => array(
      	'description' => 'The return code from the Sprawk API call',
        'type' => 'int',
        'size' => 'small',
        'not null' => TRUE),
      'src' => array(
      	'description' => 'The URL of the page where the text was located',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE),
      't' => array(
      	'description' => 'The text that was sent for translation',
        'type' => 'text',
        'size' => 'normal',
        'not null' => TRUE),
    ),
  );
  return $schema;
}

/**
 * Implements hook_install().
 */
function sprawk_install() {
  drupal_install_schema('sprawk');
  cache_clear_all('variables', 'cache');
}

/**
 * Implements hook_uninstall().
 */
function sprawk_uninstall() {
  // Drop tables.
  drupal_uninstall_schema('sprawk');
  // Remove variables.
  db_query("DELETE FROM {variable} WHERE name LIKE 'sprawk_%'");
  cache_clear_all('variables', 'cache');
}
