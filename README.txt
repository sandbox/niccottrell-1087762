What is the Sprawk module?
----------------------------

The Sprawk Module is designed to make translation of your site content and menus
very easy. Rather than "polluting" your localization table with user-gernated
content, the Sprawk module catches and sends your HTML content to an external
server for translation. Unlike Google translate, Sprawk records all translations
requests, automatically identifying important strings missing translations and
queueing them up so that you can translate them yourself or hire a freelance
translator.
The Sprawk module caches all requests locally to speed up the response times.
This module requires an account on sprawk.com and is free for non-profit sites.


Requirements
------------

- Drupal 6.x
- PHP 5.x (This module uses PHP5 JSON functionality)
- "Locale and "Content translation" modules enabled
- Apache web server with ProxyPass enabled

We recommend that you have the jQuery update module installed.


Steps to get started
---------------------

1. Extract the sprawl module into /sites/all/modules

2. Add ProxyPass rules to VirtualHost declaration:

    ProxyPass /sprawk/    http://www.sprawk.com/

    And then restart Apache for the changes to take effect. This allows jQuery
    to query the Sprawk site without violating cross-domain security.

2. Download cache.js from https://github.com/monsur/jscache and save it in the
   /sites/all/modules/sprawk folder

3. Install and enable the language module.

4. Visit /admin/settings/language and configure the languages you want available
   on your site.

5. Visit /admin/settings/language/configure and enable "Path prefix with
   language fallback."

6. Visit /admin/settings/sprawk and enter you Sprawk team code and
   authentication key

7. Log in to www.sprawk.com and view Team > Translation > Recent Translations to
   monitor translations occuring

8. Adjust your theme as desired by calling the content translation ct() function
   (see below).


How do I translate my content?
------------------------------

You can use Sprawk to translate an entire site (all menus, content etc.) or
simple translate blocks or certain parts of your layout.

For example, you could translate all blocks by editing the block.tpl.php of your
theme like this:

  <?php if (!empty($block->subject)) { ?>
  <h2 class="heading title"><?php
      print sprawk_ct($block->subject, 'Block' );
  ?></h2>
  <?php } ?>
    <div class="content"><?php print sprawk_ct($block->content); ?></div>

Notice that the sprawk_ct() function (content translation) is used rather than
the normal t() function.

You can use a similar technique to wrap blocks of HTML code in your
page.tpl.php, node.tpl.php or comment.tpl.php. It is better from a performance
point-of-view to call ct on larger blocks since caching will reduce calls to the
Sprawk server (and thus reduce lag).

Sprawk automatically detects pre-translated subsets of text, so it isn't a big
problem if some parts of the HTML sent to the Sprawk server are already
translated by Drupal t() function. One option is to export all your Drupal
localization to a PO file, and import that into Sprawk. Then clear Drupal
database localization table so that no translation takes place when t() is
called.

Translating with filters
------------------------

To easy translate content, there is a Sprawk filter available. You need to visit
/admin/settings/filters and "configure" each filter you want to be able have
translated automatically. Normally you will want "Filtered HTML", "Full HTML"
and "Plain text" enabled, but not for PHP code. The Sprawk module automatically
skips content containing "<?php " so if you will need to explicitly use the ct()
function to translation blocks of HTML content.

It is usually necessary to REORDER the filter ordering. (There is an option for
this in each input filter's "configure" page). The "Translate text" filter
should usually be towards the end of the list. The Sprawk translation engine is
expecting correctly formed HTML so any filters that convert non-HTML markup
should definitely be BEFORE the Translate text filter.

Occasionally a filter should run after the translate filter. The Glossary module
filter (which markups up term with links to definitions) should markup the
translated text, so should be run AFTER.


Translating with parameters
---------------------------

The sprawk_ct() function is very similar to the standard t() function, but the
first parameter is the "topic". This is a string which corresponds to the Topic
in the Sprawk interface. This parameter lets you translate the same source
language string differently depending on its function on your site - e.g. in the
menu or as a heading.

  sprawk_ct('Welcome', 'Menu');

or with parameters:

  sprawk_ct('Welcome !name', 'Heading', array('!name' => "John Smith"));

Translating in Javascript
-------------------------

If you want to make use of the Javascript translation t() function or the async
translation aT() you need to configure Apache to proxy pass requests to the
Sprawk web site. This is necessary because modern browsers protect against cross
domain scripting - i.e. your site cannot make requests to the sprawk.com site
directly. By configuring the web server like this:

    # Setup proxy pass for sprawk javascript
    ProxyPass    /sprawk/    http://www.sprawk.com/
    ProxyPassReverse    /sprawk/    http://www.sprawk.com/

The sprawk javascript in the module makes calls to <localhost>/sprawk/api/ which
are passes automatically to www.sprawk.com/api/
